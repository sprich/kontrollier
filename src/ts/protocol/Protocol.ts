
export namespace Protocol {

    export type AudioDevice = {
        id: number;
        name: string;
    };

    //

    export type AudioDeviceListsMessage = {
        type: "audioDeviceLists";
        inputAudioDevices: AudioDevice[];
        outputAudioDevices: AudioDevice[];
        inputAudioDeviceId: number;
        outputAudioDeviceId: number;
    };

    export type UseAudioDevicesMessage = {
        type: "useAudioDevices";
        inputAudioDeviceId: number;
        outputAudioDeviceId: number;
    };

    export type ServerInfoMessage = {
        type: "serverInfo",
        ip: string,
        port: number
    }

    //

    export type Message = AudioDeviceListsMessage | UseAudioDevicesMessage;

}
