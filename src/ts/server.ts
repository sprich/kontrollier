import * as fs from 'fs';
import * as https from 'https';
import * as ws from 'ws';
import {Protocol} from './protocol/Protocol';
import {NeverError} from "./util/NeverError";
import * as http from "http";
import ServerInfoMessage = Protocol.ServerInfoMessage;
import {v4 as uuidv4} from 'uuid';

const LISTEN_PORT = 8484;

const httpsServer = http.createServer();

type CustomWebSocket = ws & { id: string; };

let clientFeschStore = new Map<string, Set<CustomWebSocket>>();

const websocketServer = new ws.Server({server: httpsServer});

function send(websocket: ws, message: Protocol.Message) {
    websocket.send(JSON.stringify(message));
}

function sendToAllButOwnConnection(userIdentifier : string, websocket : CustomWebSocket, messageObject : Protocol.Message) {
    clientFeschStore.get(userIdentifier)?.forEach((connection) => {
        if (connection.id !== websocket.id) {
            send(connection, messageObject)
        }
    });
}

websocketServer.on('connection', (websocket_, request) => {
    let websocket = websocket_ as CustomWebSocket;
    websocket.id = uuidv4();

    const userIdentifier = request.url || "";

    console.log("got connection on " + userIdentifier);
    if (userIdentifier !== null) {
        let currentClients = clientFeschStore.get(userIdentifier);
        if (currentClients !== undefined) {
            // add current connection to the pool<
            if (currentClients.size === 1) {
                console.log("second connection for identifier");
                clientFeschStore.set(userIdentifier, currentClients.add(websocket));
            } else {
                console.log("There should only be one Fesch and one Client for each User")
            }
        } else {
            console.log("first connection for identifier");
            clientFeschStore.set(userIdentifier, new Set<CustomWebSocket>().add(websocket));
        }
    } else {
        // TODO: maybe implement something like this for websocket reconnection
        // https://github.com/pladaria/reconnecting-websocket/blob/master/README.md#update-url
    }

    let serverInfoMessage: ServerInfoMessage = {"type": "serverInfo", "ip": "127.0.0.1", "port": 80};
    websocket.send(JSON.stringify(serverInfoMessage));

    websocket.on('message', (message) => {
        let messageObject = JSON.parse(message.toString()) as Protocol.Message;

        if (typeof messageObject !== "object" || typeof (messageObject.type as any) !== "string") {
            throw new Error(`invalid message`);
        }

        switch (messageObject.type) {
            case "audioDeviceLists": {
                console.log("Received device list");
                sendToAllButOwnConnection(userIdentifier, websocket, messageObject);
                break;
            }
            case "useAudioDevices": {
                console.log("Received audio device");
                sendToAllButOwnConnection(userIdentifier, websocket, messageObject);
                break;
            }
            default: {
                throw new NeverError(messageObject, `invalid message type: ${(messageObject as any).type}`);
            }
        }
    });
});

//

httpsServer.listen(LISTEN_PORT, () => {
    console.log(`HTTPS server listening on port ${LISTEN_PORT}`);
});
