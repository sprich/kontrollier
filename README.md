# kontrollier

This is the websocket server that relays messages between belonging-together `sprich-client` instances (the native client running on user's machines) and `fesch` instances (the web site used as GUI to control `sprich-client` instances).

## Compiling
``` bash
npm run tsc-watch
```

## Starting
``` bash
npm start
```
